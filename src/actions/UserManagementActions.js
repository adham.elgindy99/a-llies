/**
 * Alle Actions im Zsmhang mit dem User Management
 * Es müssen: 
 *  alle Nutzer abgerufen werden
 *  Nutzer editiert werden
 *  Nutzer gelöscht werden
 */

 export function getShowEditUserFormAction() {
    return {
        type: 'SHOW_EDIT_USER_FORM'
    }
}

export function getHideEditUserFormAction() {
    return {
        type: 'HIDE_EDIT_USER_FORM'
    }
}

export function getShowDeleteUserFormAction() {
    return {
        type: 'SHOW_DELETE_USER_FORM'
    }
}

export function getHideDeleteUserFormAction() {
    return {
        type: 'HIDE_DELETE_USER_FORM'
    }
}

export function getShowAddUserFormAction() {
    return { type: 'SHOW_ADD_USER_FORM' }
}

export function getHideAddUserFormAction() {
    return { type: 'HIDE_ADD_USER_FORM' }
}

export function setUserToEditAction(userToEdit) {
    return {
        type: 'SET_USER_TO_EDIT',
        userToEdit: userToEdit
    }
}

export function getShowUserAlertAction(errorMessage) {
    console.log(`DISPATCH: ${ errorMessage }`)
    return {
        type: 'SHOW_ALERT',
        errorMessage: errorMessage
    }
}

export function getHideUserAlertAction() {
    return {
        type: 'HIDE_ALERT'
    }
}