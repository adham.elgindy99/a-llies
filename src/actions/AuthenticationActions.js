/* eslint-disable */
export const SHOW_LOGIN_DIALOG = "SHOW_LOGIN_DIALOG";
export const HIDE_LOGIN_DIALOG = "HIDE_LOGIN_DIALOG";

export const SHOW_LOGOUT_DIALOG = "SHOW_LOGOUT_DIALOG";
export const HIDE_LOGOUT_DIALOG = "HIDE_LOGOUT_DIALOG";

export const AUTHENTICATION_PENDING = "AUTHENTICATION_PENDING";
export const AUTHENTICATION_SUCCESS = "AUTHENTICATION_SUCCESS";
export const AUTHENTICATION_ERROR = "AUTHENTICATION_ERROR";

export function getShowLoginDialogAction() {
  return {
    //returns object that has this type
    type: SHOW_LOGIN_DIALOG,
  };
}

export function getHideLoginDialogAction() {
  return {
    //returns object that has this type
    type: HIDE_LOGIN_DIALOG,
  };
}

export function getAuthenticatedUserPendingAction() {
  return {
    //returns object that has this type
    type: AUTHENTICATION_PENDING,
  };
}

export function getAuthenticatedUserSuccessAction(userSession) {
  return {
    //these info from the userSession object will be sent to the state in root reducer
    type: AUTHENTICATION_SUCCESS,
    user: userSession.user,
    // token that was loaded from the header
    accessToken: userSession.accessToken,
  };
}

export function getAuthenticationUserErrorAction(error) {
  return {
    //returns object that has this type
    type: AUTHENTICATION_ERROR,
    error: error,
  };
}

// get userID and password from user widget
export function authenticateUser(userID, password) {
  console.log("Authenticating user");
  return (dispatch) => {
    //start the process
    dispatch(getAuthenticatedUserPendingAction());
    login(userID, password)
      .then(
        (userSession) => {
          //success
          const action = getAuthenticatedUserSuccessAction(userSession);
          dispatch(action);
        },
        //error
        (error) => {
          dispatch(getAuthenticationUserErrorAction(error));
        }
      )
      .catch((error) => {
        dispatch(getAuthenticationUserErrorAction(error));
      });
  };
}

function login(userID, password) {
  var user = userID + ":" + password;
  var userData = window.btoa(user);
  //
  // var userData = Buffer.from(user, "utf8").toString("base64");
  //same as the request options and url in the backend
  const requestOptions = {
    method: "GET",
    headers: {
      Authorization: "Basic " + userData,
    },
    // header: {'Content-Type': 'application/json'},
    // body: JSON.stringify({userID, password})
  };
  // const requestOptions = {
  //   method: "POST",
  //   headers: { "Content-Type": "application/json" },
  //   body: JSON.stringify({ userID, password }),
  // };

  return (
    fetch("http://localhost:8080/authenticate", requestOptions)
      // fetch("http://localhost:3000/login", requestOptions)
      //contains the userdata from backend
      .then(handleResponse)
      .then((userSession) => {
        return userSession;
      })
  );
}

function logout() {
  console.error("LOGOUT !!");
}

function handleResponse(response) {
  const authorizationHeader = response.headers.get("Authorization");

  // get the text from the backend json body
  return response.text().then((text) => {
    console.log("recieve result:", authorizationHeader);

    //parse user data
    const data = text && JSON.parse(text);
    var token;
    if (authorizationHeader) {
      //after the word BEARER
      token = authorizationHeader.split(" ")[1];
    }
    if (!response.ok) {
      if (response.status === 401) {
        logout();
      }
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    } else {
      //pack all data from backend in one object
      let userSession = {
        user: data,
        accessToken: token,
      };
      return userSession;
    }
  });
}

// Logout Process
export const LOGOUT_USER = "LOGOUT_USER";

export function getLogoutUserAction() {
  return {
    type: LOGOUT_USER,
  };
}
