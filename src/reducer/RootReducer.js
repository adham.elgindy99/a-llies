/* eslint-disable */
import * as authenticationActions from "../actions/AuthenticationActions";
import { LOGOUT_USER } from "../actions/AuthenticationActions";
//reducer makes the actions work and sets intial state
const initialState = {
  user: null,
  loginPending: false,
  showLoginDialog: false,
  error: null,
  userIsLoggedIn: false,
  accessToken: null,
  userToEdit: null,
  showDeleteUserDialog: false,
  showEditUserDialog: false,
  showAddUserDialog: false,
  // userIsAdmin: false
};
function rootReducer(state = initialState, action) {
  console.log("reducer: ", action.type); //bzw clicking the button

  switch (action.type) {
    case authenticationActions.SHOW_LOGIN_DIALOG:
      return {
        ...state,
        showLoginDialog: true,
        error: null,
      };
    case authenticationActions.HIDE_LOGIN_DIALOG:
      return {
        ...state,
        showLoginDialog: false,
        error: null,
      };

    case authenticationActions.AUTHENTICATION_PENDING:
      return {
        ...state,
        pending: true,
        error: null,
      };
    case authenticationActions.AUTHENTICATION_SUCCESS: {
      return {
        ...state,
        showLoginDialog: false,
        pending: false,
        user: action.user,
        accessToken: action.accessToken,
        userIsLoggedIn: true,
      };
    }
    case authenticationActions.AUTHENTICATION_ERROR: {
      return {
        ...state,
        pending: false,
        error: "Authentication failed",
      };
    }
    case LOGOUT_USER: {
      return {
        user: null,
        userIsLoggedIn: false,
        accessToken: null,
        Pending: false,
      };
    }
    case "SHOW_DELETE_USER_FORM": {
      return {
        ...state,
        showDeleteUserDialog: true,
      };
    }
    case "HIDE_DELETE_USER_FORM": {
      return {
        ...state,
        showDeleteUserDialog: false,
      };
    }
    case "SHOW_EDIT_USER_FORM": {
      return {
        ...state,
        showEditUserDialog: true,
      };
    }
    case "HIDE_EDIT_USER_FORM": {
      return {
        ...state,
        showEditUserDialog: false,
      };
    }
    case "SHOW_ADD_USER_FORM": {
      return {
        ...state,
        showAddUserDialog: true,
      };
    }
    case "HIDE_ADD_USER_FORM": {
      return {
        ...state,
        showAddUserDialog: false,
      };
    }
    case "SET_USER_TO_EDIT": {
      return {
        ...state,
        userToEdit: action.userToEdit,
      };
    }
    default:
      return state;
  }
}
export default rootReducer;
