import { Form, Button, Modal } from "react-bootstrap";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getHideEditUserFormAction,
  getShowUserAlertAction,
} from "../actions/UserManagementActions";

const EditUserWidget = ({ users, setUsers, refetch, setRefetch }) => {
  // Basic ErrorMessage. Gets specified by appending the error
  let errorMessage = "Use could not be updated. ";

  // Redux State
  const accessToken = useSelector((state) => state.accessToken);
  let userToEdit = useSelector((state) => state.userToEdit);
  let showDialog = useSelector((state) => state.showEditUserDialog);

  // Dispatch Actions
  const dispatch = useDispatch();
  const hideDialog = () => dispatch(getHideEditUserFormAction());
  const showAlert = (errorMessage) =>
    dispatch(getShowUserAlertAction(errorMessage));

  // Values for Form
  const userID = userToEdit.userID;
  const [userName, setUserName] = useState(userToEdit.userName);
  const [password, setPassword] = useState(userToEdit.password);
  const [isAdministrator, setIsAdmin] = useState(userToEdit.isAdministrator);

  // sets values for PUT-Request
  function handleChange(event) {
    event.preventDefault();
    const { name, value } = event.target;
    if (name === "edit_username_field") setUserName(value);
    if (name === "edit_password_field") setPassword(value);
    if (name === "edit_is_admin_field") {
      if (value === "admin") setIsAdmin(true);
      else setIsAdmin(false);
    }
  }

  // sends PUT-Request based on Input set in handleChange and updates UI
  // not working because of the database connection
  function handleUpdate() {
    // values here are the correct updated values
    const updatedUser = { userID, userName, password, isAdministrator };
    const putRequest = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedUser),
    };

    // console.log(putRequest);

    // fetch(`https://localhost:443/user`, putRequest)
    fetch("http://localhost:8080/users", putRequest)
      .then(handleResponse)
      .then(setRefetch(!refetch))
      .then(hideDialog());
  }

  /**
   * handles Response to DELETE-Request and shows Alert
   * with descriptive message if something went wrong
   */
  function handleResponse(response, updatedUser) {
    console.log(response);

    if (!response.ok) {
      if (response.status === 401) {
        errorMessage += "You are not authorized.";
        console.error(errorMessage);
        showAlert(errorMessage);
        return;
      }
      if (response.status === 500) {
        errorMessage += "Server Error.";
        console.error(errorMessage);
        showAlert(errorMessage);
        return;
      }
    }
  }

  // // removes old instance of User and adds the updated one to UI
  // function updateUI(updatedUser) {
  //     console.log("EDIT VORHER" + users)
  //     setUsers(users.filter((user) => user.userID !== updatedUser.userID))
  //     console.log("EDIT NACHHER" + users)
  //     setUsers(setUsers([...users, updatedUser]))
  //     console.log("EDIT ENDE" + users)
  // }

  return (
    <div>
      <Modal show={showDialog} onHide={hideDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Edit User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mb-3">
            <Form.Label>User ID</Form.Label>
            <Form.Control
              placeholder={userToEdit.userID}
              id="UserIDInput"
              disabled
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>User Name</Form.Label>
            <Form.Control
              type="Username"
              id="UserNameInput"
              name="edit_username_field"
              defaultValue={userToEdit.userName}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              id="PasswordInput"
              name="edit_password_field"
              defaultValue={userToEdit.password}
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Select
            aria-label="Default select example"
            name="edit_is_admin_field"
            defaultValue={userToEdit.isAdministrator ? "admin" : "user"}
            onChange={handleChange}
          >
            <option value="user">User</option>
            <option value="admin">Administrator</option>
          </Form.Select>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideDialog}>
            Close
          </Button>
          <Button variant="primary" id="SaveUserButton" onClick={handleUpdate}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default EditUserWidget;
