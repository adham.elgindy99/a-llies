/* eslint-disable */
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import LogoutButton from "./LogoutButton";

import * as authenticationActions from "../actions/AuthenticationActions";
import { getLogoutUserAction } from "../actions/AuthenticationActions";

const mapStateToProps = (state) => {
  return state;
};

class UserSessionWidget extends Component {
  constructor(props) {
    super(props);
    // this.state = { show: false };
    this.state = { username: "", password: "" };
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleShow(e) {
    e.preventDefault();
    //to rerun the render method and save changes
    //better than setting a local state is sending the action to redux aand getting back the info
    //this.setState({ show: true });
    const { showLoginDialogAction } = this.props;
    showLoginDialogAction();
  }

  handleClose() {
    //better than setting a local state is sending the action to redux aand getting back the info
    //this.setState({ show: false });
    const { hideLoginDialogAction } = this.props;
    hideLoginDialogAction();
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    //consol.log(JSON.stringify(this.state));
  }

  handleSubmit(e) {
    e.preventDefault();
    // get data from state
    const { username, password } = this.state;
    console.log(username);
    console.log(password);
    const { authenticateUserAction } = this.props;
    authenticateUserAction(username, password);
    console.log("submit");
  }

  render() {
    var showDialog = this.props.showLoginDialog;
    if (showDialog === undefined) showDialog = false;
    const user = this.props.user;
    // console.log("showDialog user: ", user); # "token created succefully"
    return (
      <div>
        {user ? (
          <LogoutButton />
        ) : (
          <Button variant="primary" onClick={this.handleShow}>
            Login
          </Button>
        )}
        {/* <Button variant="primary" onClick={this.handleShow}>
          Login
        </Button> */}
        <Modal show={showDialog} onHide={this.handleClose}>
          <Modal.Header onClick={this.handleClose} closeButton>
            <Modal.Title>User login</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  id="LoginUserIDInput"
                  placeholder="user ID"
                  name="username"
                  onChange={this.handleChange}
                />
                {/* <Form.Text className="text-muted"> 
                  We'll never share your email with anyone else. 
                </Form.Text> */}
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  id="LoginPasswordInput"
                  placeholder="Password"
                  name="password"
                  onChange={this.handleChange}
                />
              </Form.Group>
              {/* <Form.Group className="mb-3" controlId="formBasicCheckbox"> 
                <Form.Check type="checkbox" label="Check me out" /> 
              </Form.Group> */}
              <Button
                variant="primary"
                id="LoginButton"
                type="submit"
                onClick={this.handleSubmit}
              >
                Submit
              </Button>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            Forgot Password ?
            {/* <Button variant="secondary" onClick={this.handleClose}> 
              Close 
            </Button> 
            <Button variant="primary" onClick={this.handleClose}> 
              Save Changes 
            </Button> */}
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

//connects action with dispatch method
const mapDispatchtoProps = (dispatch) =>
  bindActionCreators(
    {
      showLoginDialogAction: authenticationActions.getShowLoginDialogAction,
      hideLoginDialogAction: authenticationActions.getHideLoginDialogAction,
      authenticateUserAction: authenticationActions.authenticateUser,
    },
    dispatch
  );

const ConnectedUserSessionWidget = connect(
  mapStateToProps,
  mapDispatchtoProps
)(UserSessionWidget);

export default ConnectedUserSessionWidget;
