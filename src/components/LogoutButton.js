/* eslint-disable */
import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

import { connect } from "react-redux";

import {
  getShowLogoutDialogAction,
  getLogoutUserAction,
} from "../actions/AuthenticationActions";

class LogoutButton extends Component {
  constructor(props) {
    super(props);
    // this.showLoginDialog = this.showLoginDialog.bind(this);
    this.logoutuserAction = this.logoutuserAction.bind(this);
  }

  //
  logoutuserAction(event) {
    event.preventDefault();
    // window.location.href = "http://localhost:3000/publicPage";
    const dispatch = this.props.dispatch;
    dispatch(getLogoutUserAction());
  }

  render() {
    return (
      <div>
        <Button
          id="LogoutButton"
          variant="primary"
          onClick={this.logoutuserAction}
        >
          Logout
        </Button>
      </div>
    );
  }
}

//connect the login button
export default connect()(LogoutButton);
