/* eslint-disable */
import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavDropdown from "react-bootstrap/NavDropdown";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
// import Form from 'react-bootstrap/Form'
// import FormControl from 'react-bootstrap/FormControl'
// import Button from 'react-bootstrap/Button'
import UserSessionWidget from "./UserSessionWidget";

import { useSelector } from "react-redux";

// const user = useSelector((state) => state.userIsLoggedIn);

const NavBar = () => {
  const userName = useSelector((state) => state.user);
  // console.log(userName);
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Container fluid>
          <Navbar.Brand href="#">Menu</Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="me-auto my-2 my-lg-0"
              style={{ maxHeight: "100px" }}
              navbarScroll
            >
              <Nav.Link as={Link} to={"/"}>
                Home{" "}
              </Nav.Link>
              <Nav.Link href="#action2">About Us</Nav.Link>
              <NavDropdown title="Subscribe" id="navbarScrollingDropdown">
                <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action4">
                  Another action
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action5">
                  Something else here
                </NavDropdown.Item>
              </NavDropdown>

              {/*should only be visible when user isAdmin*/}
              <Nav.Link
                as={Link}
                to="/userManagement"
                id="OpenUserManagementButton"
              >
                <i className="fa fa-user mr-2 fa-fw activityBarItem"></i>
                User Management
              </Nav.Link>

              <Nav.Link href="#" disabled>
                GmbH
              </Nav.Link>
            </Nav>
            <UserSessionWidget />
            {/* <Form className="d-flex">
        <FormControl
          type="search"
          placeholder="Search"
          className="me-2"
          aria-label="Search"
        />
        <Button variant="outline-success">Search</Button>
      </Form> */}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavBar;
