import React from "react";
import { Form, Button, Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  getHideAddUserFormAction,
  getShowUserAlertAction,
} from "../actions/UserManagementActions";
import { useState } from "react";

const AddUserWidget = ({ users, setUsers }) => {
  // Basic ErrorMessage. Gets specified by appending the error
  let errorMessage = "User could not be added. ";

  // Redux State
  const accessToken = useSelector((state) => state.accessToken);
  let showDialog = useSelector((state) => state.showAddUserDialog);

  // Dispatch Actions
  const dispatch = useDispatch();
  const hideDialog = () => dispatch(getHideAddUserFormAction());
  const showAlert = (errorMessage) =>
    dispatch(getShowUserAlertAction(errorMessage));

  // Values for Form
  const [userID, setUserID] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [isAdministrator, setIsAdmin] = useState(false);

  // sets values for POST-Request
  function handleChange(event) {
    event.preventDefault();
    const { name, value } = event.target;
    if (name === "add_userid_field") setUserID(value);
    if (name === "add_username_field") setUserName(value);
    if (name === "add_password_field") setPassword(value);
    if (name === "add_is_admin_field") {
      if (value === "admin") setIsAdmin(true);
      else setIsAdmin(false);
    }
  }

  // sends POST-Request based on Input set in handleChange and updates UI
  function handlePost() {
    const newUser = { userID, userName, password, isAdministrator };
    // console.log("is admin: ", isAdministrator);
    // Request Body wird dynamisch ausgefüllt
    const postRequest = {
      method: "POST",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newUser),
    };

    console.log(postRequest);

    // fetch(`https://localhost:443/user`, postRequest)
    fetch("http://localhost:8080/users", postRequest)
      .then(handleResponse)
      // updates UI by adding the new User to users Array -> no new fetch needed
      //TODO: nur ausführen, wenn kein Error aufgetreten ist
      .then(setUsers([...users, newUser]))
      .then(hideDialog());
  }

  /**
   * handles Response to POST-Request and shows Alert
   * with descriptive message if something went wrong
   */
  function handleResponse(response) {
    console.log(response);

    if (!response.ok) {
      if (response.status === 401) {
        errorMessage += "You are not authorized.";
        console.error(errorMessage);
        // showAlert(errorMessage);
        return;
      }
      if (response.status === 500) {
        errorMessage += "User might already exist";
        console.error(errorMessage);
        // showAlert(errorMessage);
        return;
      }
    }
  }

  return (
    <div>
      <Modal show={showDialog} onHide={hideDialog}>
        <Modal.Header closeButton>
          <Modal.Title>Add User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group className="mb-3">
            <Form.Label>User ID</Form.Label>
            <Form.Control
              placeholder="Unique Identifier for a User..."
              id="UserIDInput"
              name="add_userid_field"
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>User Name</Form.Label>
            <Form.Control
              type="Username"
              id="UserNameInput"
              name="add_username_field"
              placeholder="Real name of user..."
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              id="PasswordInput"
              name="add_password_field"
              onChange={handleChange}
            />
          </Form.Group>
          <Form.Select
            aria-label="Default select example"
            name="add_is_admin_field"
            defaultValue="user"
            onChange={handleChange}
          >
            <option value="user">User</option>
            <option value="admin">Administrator</option>
          </Form.Select>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={hideDialog}>
            Close
          </Button>
          <Button id="CreateUserButton" variant="primary" onClick={handlePost}>
            Add User
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default AddUserWidget;
