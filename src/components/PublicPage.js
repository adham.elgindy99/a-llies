/* eslint-disable */
import React from "react";
import "../styles/css/layout.css";
import logo from "../alienLogo.png";
import LoginButton from "./LoginButton";

const PublicPage = () => {
  return (
    <div
      id="LandingPage"
      className="page-content"
      style={{
        backgroundColor: "#282c34",
        width: "100%",
        height: "100%",
        color: "white",
      }}
    >
      <div>
        <div style={{ paddingTop: "2em" }}></div>
        <p
          style={{
            fontSize: "3rem",
          }}
        >
          <b>A-llies</b>
        </p>

        {/* <button>Login</button> */}
      </div>
      <LoginButton />
      <div
        style={{
          fontSize: "calc(10px + 2vmin)",
          padding: "3em",
        }}
      >
        <img
          style={{ padding: "3em" }}
          src={logo}
          className="App-logo"
          alt="logo"
        />
        <p>Start chatting with your first alien buddy</p>
        <a
          className="App-link"
          href="https://news.harvard.edu/gazette/story/2022/03/how-to-talk-to-extraterrestrials/"
          target="_blank"
          rel="noopener noreferrer"
        >
          The first interactive form to communicate with aliens
        </a>
      </div>
      <div>
        communicating with aliens is a very important step for mankind that we
        should cherish nowadays. As our world is changing and we are
        communicating with other worlds, we have to understand how they live and
        how they think about how we live.
      </div>
    </div>
  );
};

export default PublicPage;
