import React from "react";
import { Button } from "react-bootstrap";
import {
  getShowEditUserFormAction,
  getShowDeleteUserFormAction,
  setUserToEditAction,
} from "../actions/UserManagementActions";
import { useDispatch } from "react-redux";
import pp from "../profilePic.png";

const User = ({ user, users, setUsers }) => {
  // Dispatch Actions
  const dispatch = useDispatch();
  const showEditUserDialog = () => dispatch(getShowEditUserFormAction());
  const showDeleteUserDialog = () => dispatch(getShowDeleteUserFormAction());
  const setUserToEdit = (userToEdit) =>
    dispatch(setUserToEditAction(userToEdit));

  /**
   * searches the array of users for the user that triggered the event
   * sets the userToEdit which is a property that EditUserWidget and DeleteUserWidget access
   * to show the correct values to edit
   * based on the event source it opens the EditUserWidget or DeleteUserWidget
   */
  function onEdit(event) {
    let userToEdit = users.find(
      (userToFind) => userToFind.userID === user.userID
    );
    console.log("USER TO EDIT: " + JSON.stringify(userToEdit));
    setUserToEdit(userToEdit);

    if (event.target.id.includes("EditButton")) showEditUserDialog();
    else showDeleteUserDialog();
  }

  return (
    <>
      <div
        style={{
          color: "white",
          padding: "1em",
          alignItems: "center",
        }}
        className="user_card"
        id={`UserItem${user.userID}`}
      >
        {/* <img src="./profilePic.png" alt="profile_picture" /> */}

        <ul>
          <li>
            <img
              style={{ padding: "1em", height: "150px", width: "150px" }}
              src={pp}
              alt="logo"
              borderRadius="100"
            />
          </li>
          <li>{user.userID}</li>
          <li>Name: {user.userName}</li>
          <li>Rolle: {user.isAdministrator ? "Administrator" : "User"}</li>
          <Button
            id={`EditButton${user.userID}`}
            variant="primary"
            className="m-5"
            onClick={onEdit}
          >
            Edit
          </Button>
          <Button
            id={`DeleteButton${user.userID}`}
            variant="warning"
            className="m-5"
            onClick={onEdit}
          >
            Delete
          </Button>
        </ul>
      </div>
    </>
  );
};

export default User;
