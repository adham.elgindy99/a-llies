/* eslint-disable */
import React from "react";
import "../styles/css/layout.css";
import LogoutButton from "./LogoutButton";
import alien from "../aliennn.png";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

const PrivatePage = () => {
  return (
    <div
      className="page-content"
      id="PrivatePage"
      style={{
        backgroundColor: "#282c34",
        width: "100%",
        height: "100%",
        color: "white",
        fontSize: "calc(10px + 2vmin)",
        padding: "1em",
      }}
    >
      <h1>welcome to the private page</h1>
      <Button style={{ margin: "1em" }} to="/" id="OpenPrivatePageButton">
        Private start page
        <i className="fa fa-home mr-3 fa-fw activityBarItem"></i>
      </Button>
      <LogoutButton />

      <img
        style={{ padding: "2em" }}
        height="450px"
        width="350px"
        src={alien}
        className="alien"
        alt="alien"
      />

      <h2>Online now</h2>
      <Table striped bordered hover variant="dark">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Red</td>
            <td>Dwarf</td>
            <td>@rdffo</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Frontend</td>
            <td>Master</td>
            <td>@Jedi</td>
          </tr>
          <tr>
            <td>3</td>
            <td colSpan={2}>Larry the Bird</td>
            <td>@tweetTweet</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default PrivatePage;
