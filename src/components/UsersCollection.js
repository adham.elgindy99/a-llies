import React from 'react'
import User from './User'

const UsersCollection = ({ users, setUsers }) => {
    return (
        <>
            {users > 0 ? (<h3>No Users to Show</h3>) :
                (<div id="card_container">
                    {users.map(user => {
                        return <User
                            key={user.userID}
                            user={user}
                            users={users}
                            setUsers={setUsers} />
                    })}
                </div>)
            }
        </>
    )
}

export default UsersCollection
