import { Button, Modal } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import { getHideDeleteUserFormAction, getShowUserAlertAction } from '../actions/UserManagementActions';


const DeleteUserWidget = ({ users, setUsers }) => {
    // Basic ErrorMessage. Gets specified by appending the error
    let errorMessage = "User could not be deleted. "

    // Redux State
    const accessToken = useSelector(state => state.accessToken)
    let userToDelete = useSelector(state => state.userToEdit)
    let showDialog = useSelector(state => state.showDeleteUserDialog)

    // Dispatch Actions
    const dispatch = useDispatch()
    const hideDialog = () => dispatch(getHideDeleteUserFormAction())
    const showAlert = (errorMessage) => dispatch(getShowUserAlertAction(errorMessage))

    // sends DELETE-Request and updates UI
    function handleDelete() {
        const deleteRequest = {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${ accessToken }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ "userID": `${ userToDelete.userID }` })
        }

        console.log(deleteRequest)

        // fetch(`https://localhost:443/user`, deleteRequest)
        fetch("http://localhost:8080/users", deleteRequest)
            .then(handleResponse)
            // UI will only show users that do not match the given userID
            .then(setUsers(users.filter((user) => user.userID !== userToDelete.userID)))
            .then(hideDialog())
    }

    /**
     * handles Response to DELETE-Request and shows Alert 
     * with descriptive message if something went wrong
     */
    function handleResponse(response) {
        console.log(response)

        if (response.status === 401) {
            errorMessage += "You are not authorized."
            console.error(errorMessage)
            showAlert(errorMessage)
            return
        }
        if (response.status === 500) {
            errorMessage += "Server Error. "
            console.error(errorMessage)
            showAlert(errorMessage)
            return
        }
    }

    return (
        <div>
            <Modal show={showDialog} onHide={hideDialog}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete User {userToDelete.userID}?</Modal.Title>
                </Modal.Header>
                <Modal.Body><p>You are about to delete this User. This action can not be undone.</p>
                    Are you sure you want to proceed? </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={hideDialog}>
                        Close
                    </Button>
                    <Button variant="danger" onClick={handleDelete}>
                        Delete User
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default DeleteUserWidget