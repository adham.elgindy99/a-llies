import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { getShowAddUserFormAction } from "../actions/UserManagementActions";
import AddUserWidget from "./AddUserWidget";
import UsersCollection from "./UsersCollection";
import DeleteUserWidget from "./DeleteUserWidget";
import EditUserWidget from "./EditUserWidget";
// import UserActionAlert from './UserActionAlert'

const UserManagement = () => {
  // Redux State
  // const showAlert = useSelector(state => state.showAlert)
  //selector function that recieves redux state and returns value
  const userIsLoggedIn = useSelector((state) => state.userIsLoggedIn);
  let accessToken = useSelector((state) => state.accessToken);
  let userToEdit = useSelector((state) => state.userToEdit);
  //   let user = useSelector((state) => state.user);
  //   console.log("fuck: ", user);
  console.log("userToEdit: ", userToEdit);

  // Dispatch Actions
  const dispatch = useDispatch();
  function showAddUserDialog() {
    dispatch(getShowAddUserFormAction());
  }

  /**
   * Array of Users
   * gets modified in the widgets to visibly reflect API State without having to reload data
   */
  const [users, setUsers] = useState([]);
  // Dependency for useEffect: Value is inverted after EditUser has finished, so the values are updated
  const [refetch, setRefetch] = useState(false);

  /**
   * Fetches GET-Request to Backend after Component has mounted
   * Saves the Response (all Users) in users Array
   * AccessToken Dependency necessary
   */
  useEffect(() => {
    console.log("user: ", setUsers);
    // fetch(`https://localhost:443/user`, {
    fetch("http://localhost:8080/users", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((response) => response.json())
      .then((json) => setUsers(json));
  }, [accessToken, refetch]);

  return (
    <>
      {userIsLoggedIn && (
        <div id="userManagement">
          <AddUserWidget users={users} setUsers={setUsers} />
          {userToEdit !== undefined && (
            <DeleteUserWidget users={users} setUsers={setUsers} />
          )}
          {userToEdit !== undefined && (
            <EditUserWidget
              users={users}
              setUsers={setUsers}
              refetch={refetch}
              setRefetch={setRefetch}
            />
          )}

          <h1
            style={{
              color: "white",
              padding: "1em",
            }}
          >
            User Management
          </h1>
          <UsersCollection users={users} setUsers={setUsers} />
          <div id="add_user_div">
            <Button
              id="OpenCreateUserDialogButton"
              variant="primary"
              className="m-5"
              onClick={showAddUserDialog}
            >
              Add User
            </Button>
          </div>
        </div>
      )}
    </>
  );
};

export default UserManagement;
