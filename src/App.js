/* eslint-disable */
import React, { Component } from "react";
import { connect } from "react-redux";
import "./App.css";
import PublicPage from "./components/PublicPage";
import PrivatePage from "./components/PrivatePage";
import { Link } from "react-router-dom";
// import logo from './logo.svg';
// import TopMenu from './components/TopMenu'
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import NavBar from "./components/NavBar";
import UserManagement from "./components/UserManagement";

const mapStateToProps = (state) => {
  return state;
};

class App extends Component {
  render() {
    const user = this.props.user;

    let workSpace;
    if (user) {
      workSpace = <PrivatePage />;
    } else if (!user) {
      workSpace = <PublicPage />;
      // } else {
      //   <UserManagement/>
    }

    return (
      <Router>
        <div className="App">
          <NavBar />
          <Routes>
            <Route path="/" element={workSpace} />
            <Route path="/userManagement" element={<UserManagement />} />
            {/* <Route path="*" element={<ErrorPage />} /> */}
          </Routes>
        </div>
      </Router>
    );
  }
}

export default connect(mapStateToProps)(App);

{
  /* <div className="App">
<NavBar />
{workSpace}

{
<header className="App-header">
<img src={logo} className="App-logo" alt="logo" />
<p>
Thanks for visiting the website
</p>
<a
className="App-link"
href="https://reactjs.org"
target="_blank"
rel="noopener noreferrer"
>
please login with this link that takes you to react page 
</a>
</header> }
</div> */
}
