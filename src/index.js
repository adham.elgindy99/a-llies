/* eslint-disable */
import React from "react";
import ReactDOM from "react-dom";
//import * as ReactDOM from 'react-dom';
//import { createRoot } from 'react-dom/client';

import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
// import * as serviceWorker
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

import { createStore } from "redux";
import { Provider } from "react-redux";

import thunk from "redux-thunk";
import { applyMiddleware } from "redux";
// import createRoot from "react-dom/client";

import rootReducer from "./reducer/RootReducer";

const intialState = {};
const middlewares = [thunk];

//reducer makes the actions work and sets intial state
const store = createStore(
  rootReducer,
  intialState,
  applyMiddleware(...middlewares)
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
